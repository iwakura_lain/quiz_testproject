﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AdminButton : MonoBehaviour {

	public InputField passwordField;
	public GameObject passwordFieldObj;

	public string password;

	void Start () {
		
	}

	public void ClickAdmin () {
		passwordFieldObj.SetActive(true);
	}

	public void ClickClient()
	{
		SceneManager.LoadScene("MenuScreen");
	}

	public void Pass()
	{
		if(passwordField.text == password)
			SceneManager.LoadScene("MenuScreen");

	}
}
