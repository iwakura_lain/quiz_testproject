﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnswerButton : MonoBehaviour {

	public Text answerText;

	private AnswerData answerData;
	private Controller controller;

	void Start () {
		controller = FindObjectOfType<Controller>();
	}
	
	public void Setup(AnswerData data)
	{
		answerData = data;
		answerText.text = answerData.answerText;
	}

	public void HandleClick()
	{
		controller.AnswerButtonClicked(answerData.isCorrect);
	}

}
