﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScreenController : MonoBehaviour {

	public void PlayDemo()
	{
		SceneManager.LoadScene("Demo");
	}

	public void Registration()
	{
		SceneManager.LoadScene("Registration");
	}

	public void StartQuiz()
	{
		SceneManager.LoadScene("Quiz");
	}
}
