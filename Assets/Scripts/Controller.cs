﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Controller : MonoBehaviour {

	public Text questionText;
	public SimpleObjectPool answerButtonObjectPool;
	public Transform answerButtonParent;
	public Text scoreDisplayText;
	public GameObject questionPanel;
	
	private DataController dataController;
	private RoundData currentRoundData;
	private QuestionData[] questionPool;

	private bool isRoundActive;
	private int questionIndex;
	private int Score;
	private List<GameObject> answerButtonGameObjects = new List<GameObject>();

	void Start () {
		dataController = FindObjectOfType<DataController>();
		currentRoundData = dataController.GetCurrentRoundData();
		questionPool = currentRoundData.questions;

		Score = 0;
		questionIndex = 0;

		ShowQuestion();
		//isRoundActive = true;
	}
	
	private void ShowQuestion()
	{
		RemovwAnswerButtons();
		QuestionData questionData = questionPool[questionIndex];
		questionText.text = questionData.questionText;

		for(int i = 0; i < questionData.answer.Length; i++)
		{
			GameObject answerButtonGameObject = answerButtonObjectPool.GetObject();
			answerButtonGameObject.transform.SetParent(answerButtonParent);
			answerButtonGameObjects.Add(answerButtonGameObject);

			AnswerButton answerButton = answerButtonGameObject.GetComponent<AnswerButton>();
			answerButton.Setup(questionData.answer[i]);
		}
	}

	private void RemovwAnswerButtons()
	{
		while(answerButtonGameObjects.Count > 0)
		{
			answerButtonObjectPool.ReturnObject(answerButtonGameObjects[0]);
			answerButtonGameObjects.RemoveAt(0);
		}
	}

	public void AnswerButtonClicked(bool isCorrect)
	{
		if(isCorrect)
		{
			Score += currentRoundData.pointsAddedForCorrectAnswer;
			scoreDisplayText.text = "Верно: " + Score.ToString();
		}

		if(questionPool.Length > questionIndex + 1)
		{
			questionIndex++;
			ShowQuestion();
		}
		else
		{
			End();
		}

	}

	public void End()
	{
		//isRoundActive = false;
		SceneManager.LoadScene("Statistics");
	}

	public void ReturnToMenu()
	{
		SceneManager.LoadScene("MenuScreen");
	}

	void Update () {
		
	}
}
