﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class Playback : MonoBehaviour, IDragHandler, IPointerDownHandler
{
	public VideoPlayer vidPlayer;
	private Image progress;

	void Start()
	{
		progress = GetComponent<Image>();
		vidPlayer.Play();
		vidPlayer.loopPointReached += End;
	}

	void Update()
	{
		if(vidPlayer.clip == null)
			vidPlayer.clip = Resources.Load("Assets/Media/City2_vympel.mp4") as VideoClip;

		if (vidPlayer.frameCount > 0)
			progress.fillAmount = (float)vidPlayer.frame / (float)vidPlayer.frameCount;
	}

	public void OnDrag(PointerEventData eventData)
	{
		TrySkip(eventData);
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		TrySkip(eventData);
	}

	private void TrySkip(PointerEventData eventData)
	{
		Vector2 localPoint;
		if (RectTransformUtility.ScreenPointToLocalPointInRectangle(progress.rectTransform, eventData.position, null, out localPoint))
		{
			float pct = Mathf.InverseLerp(progress.rectTransform.rect.xMin, progress.rectTransform.rect.xMax, localPoint.x);
			SkipToPercent(pct);
		}
	}

	private void SkipToPercent(float pct)
	{
		var frame = vidPlayer.frameCount * pct;
		vidPlayer.frame = (long)frame;
	}

	private void End(UnityEngine.Video.VideoPlayer vp)
	{
		vidPlayer.Stop();
		SceneManager.LoadScene("MenuScreen");
	}

}
